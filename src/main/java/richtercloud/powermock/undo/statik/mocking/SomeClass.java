package richtercloud.powermock.undo.statik.mocking;

/**
 *
 * @author richter
 */
public class SomeClass {

    public void someMethod() {
        try {
            StaticClass.staticMethod();
        }catch(SomeException ex) {
            System.out.println("SomeException occurred");
            throw new IllegalStateException(ex);
        }
        System.out.println("SomeException didn't occur");
    }
}
