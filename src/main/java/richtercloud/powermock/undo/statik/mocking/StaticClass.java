package richtercloud.powermock.undo.statik.mocking;

/**
 *
 * @author richter
 */
public class StaticClass {

    public static void staticMethod() throws SomeException {
        System.out.println("staticMethod");
    }

    private StaticClass() {
    }
}
